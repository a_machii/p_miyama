<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'aa210ax0sg_wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'aa210ax0sg');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'WHSHTbBw');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X9iwm33)E52q IY!-9f(:PHFv}nDC*xt(0-])K96F09@KX:)PrfA/hZ]A6W)K_q3');
define('SECURE_AUTH_KEY',  'Z+~h#?+ui_T>5+nL_PwGKTYsKLo06f1% 6Y@luoeQ6^<|&dCJmGvKj:w-Iy|wnTU');
define('LOGGED_IN_KEY',    '7*iy)dgj(L?)-3pP68-3FLD+n5-tEf-c/l_$D3+-nFFc9fcfhj,.#)Os_nvF65it');
define('NONCE_KEY',        's7DJ!+PB,4foeRmwIGH7-F;B|9>,U*gMYH^jCV*`7Qat-n:{Y|%:mHjl@o}Uq(+j');
define('AUTH_SALT',        'g`Im%ih6|jeL~fCp, clt_]GbUbL4Dj`p6}.V|i]wEv/||i|X./uNQR+8ra#8p:i');
define('SECURE_AUTH_SALT', 'K>=*<.^AF7rHl 9R~vac|Ru}S)g2wZ_)|RmG|_$HsS-:[F@/Dp2$X/nywiqB=jj$');
define('LOGGED_IN_SALT',   'qO}R;K(EIdH3`3!||hJ@B1Bt@%U^+?{ }GRbI%A9o-_eyy|Dw8V$,1jad;-lSN9h');
define('NONCE_SALT',       '+!*$9`3z-fK]r-J/1UgMuRj*[aYRvUz.Bs]q0p,(fg1:z;~~Ga#cP0RK]+RcDAlp');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
