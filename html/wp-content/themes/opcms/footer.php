<?php if(is_home()): ?>
  <aside>
    <div id="to_top">
      <div class="inner">
        <p><a href="#header" class="ophv"><img src="<?php echo site_url('/'); ?>common/img/page_top_btn.png" height="71" width="71" alt="ページトップ" ></a></p>
      </div><!--/.inner-->
    </div><!--/#btm_to_top-->
  </aside>

<?php else: ?>
  <aside>
    <div id="btm_to_top">
      <div class="inner">
        <p><a href="#header" class="ophv"><img src="<?php echo site_url('/'); ?>common/img/page_top_btn.png" height="71" width="71" alt="ページトップ" ></a></p>
      </div><!--/.inner-->
    </div><!--/#btm_to_top-->
  </aside>
<?php endif; ?>

  <footer>
    <div id="footer">
      <div class="inner">
        <div class="shop_info">
          <h2><img src="<?php echo site_url('/'); ?>common/img/footer_logo.png" height="109" width="180" alt="パティスリーミヤマ"></h2>
          <dl>
            <dt>住所</dt>
            <dd>〒277-0861 千葉県柏市高田1042-23</dd>
            <dt>アクセス</dt>
            <dd>市内循環バス「高田野鳥公園入口」下車</dd>
            <dt>連絡先</dt>
            <dd>TEL：04-7168-0717</dd>
            <dt>営業時間</dt>
            <dd>10：00～19：00</dd>
            <dt>定休日</dt>
            <dd>火曜日　※祝日の場合は営業いたします</dd>
          </dl>
        </div><!--/.shop_info-->

        <div class="tel">
          <p class="txt01">ケーキのご予約・お問合せはお電話にてお気軽にどうぞ！</p>
          <!--spのみ表示-->
          <p class="tel_icon rsp"><img src="<?php echo site_url('/'); ?>common/img/sp/tel.png" height="27" width="247" alt=""></p>
          <p class="txt02">Open 10:00-19:00／Close 火曜</p>
        </div><!--/.tel-->

        <!--pcのみ表示-->
        <ul class="pc">
          <li><a href="<?php echo site_url('/'); ?>" class="ophv">ホーム</a></li>
          <li><a href="<?php echo site_url('/'); ?>hallcake" class="ophv">ホールケーキ</a></li>
          <li><a href="<?php echo site_url('/'); ?>petitgateau" class="ophv">プチ・ガトー</a></li>
          <li><a href="<?php echo site_url('/'); ?>baked" class="ophv">焼き菓子</a></li>
          <li><a href="<?php echo site_url('/'); ?>shop" class="ophv">店舗案内</a></li>
          <li><a href="<?php echo site_url('/'); ?>contact" class="ophv">お問合せ</a></li>
        </ul>

        <p class="copyright">Copyright (C) Patisserie MIYAMA Kashiwa,Chiba. All Rights Reserved.</p>
      </div><!--/.inner-->
    </div><!--/#footer-->
  </footer>

  <!-- script -->
  <script type="text/javascript" src="<?php echo site_url('/'); ?>common/js/jquery-1.12.2.min.js"></script>
  <script type="text/javascript" src="<?php echo site_url('/'); ?>common/js/jquery.smoothScroll.js"></script>
  <script type="text/javascript" src="<?php echo site_url('/'); ?>common/js/rollover/opacity-rollover2.1.js"></script>
  <script type="text/javascript" src="<?php echo site_url('/'); ?>common/js/rollover/rollover.js"></script>
  <script type="text/javascript" src="<?php echo site_url('/'); ?>common/js/jquery.bxslider.min.js"></script>
  <script type="text/javascript" src="<?php echo site_url('/'); ?>common/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

  <!--smoothScrollの記述-->
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
  <script>
    jQuery(function($) {
        $('a[href^="#"]').SmoothScroll({
            duration: 1000,
            easing: 'easeOutQuint'
        });
    });
  </script>

  <!--ロールオーバーの記述-->
  <script type="text/javascript">
    $(document).ready(function() {
        $('.ophv').opOver(1.0, 0.6, 200, 200);
    });
  </script>

  <!--bxsliderの記述-->
  <script type="text/javascript">
    $(document).ready(function(){
      $('.slider').bxSlider({
        auto: true,
        pause:  5500,
        speed: 3000,
        mode: 'fade',
        pager:true,
        controls: false
      });
    });
  </script>

  <!--スクローバーの記述-->
  <script type="text/javascript">
  $(function(){
    $(".article_area").mCustomScrollbar();
  });
  </script>

  <!--レスポンシブのメニュー記述-->
  <script type="text/javascript">
    $(document).ready(function(){
      $(window).resize(function() {
          var w = $(window).width();
          if(750 <= w){
            $('#menu').css("display","none");
          }
        });
        $('#menu_btn').on('click',function(){
          $('#menu').slideToggle();
      });
    });
  </script>

<?php wp_footer(); ?>
</body>
</html>