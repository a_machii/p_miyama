<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<?php
  global $this_pagetitle;
  global $this_page_keywd;
  global $this_page_desc;
?>
  <meta charset="<?php bloginfo('charset'); ?>">
  <title><?php if($this_pagetitle) : echo $this_pagetitle. ' | '; endif; ?>柏のケーキ屋さん（ロールケーキ・キャラクターケーキ）｜パティスリーミヤマ</title>
  <meta name="description" content="<?php if($this_page_desc) : echo $this_page_desc. ' - '; elseif($this_pagetitle) : echo $this_pagetitle. ' - '; endif; ?>パティスリーミヤマは柏にてロールケーキやキャラクターケーキを提供するケーキ屋です。" >
  <meta name="keywords" content="<?php if($this_page_keywd) : echo $this_page_keywd. ','; elseif($this_pagetitle) : echo $this_pagetitle. ','; endif; ?>ケーキ屋,柏,ロールケーキ,キャラクターケーキ" >
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('/'); ?>common/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('/'); ?>common/css/reset.css">
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('/'); ?>common/js/scrollbar/jquery.mCustomScrollbar.css">
<?php if(is_page('contact')): ?>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('/'); ?>mailform/mfp.statics/mailformpro.css">
<?php endif; ?>
  <!--&#91;if lt IE 9&#93;>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <!&#91;endif&#93;-->

  <!--googleアナリティクスの記述-->
  <script type="text/javascript">
   var _gaq = _gaq || [];
   _gaq.push(['_setAccount', 'UA-20787747-1']);
   _gaq.push(['_trackPageview']);

   (function() {
     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
   })();
  </script>

  <?php wp_head(); ?>
</head>

<body>
  <header>
    <div id="header">
      <div class="inner">
        <div class="logo">
          <h1><a href="<?php echo site_url('/'); ?>">パティスリーミヤマ</a></h1>
        </div><!--/.logo-->

        <!--spのみ表示-->
        <p id="menu_btn" class="rsp"><img src="<?php echo site_url('/'); ?>common/img/sp/menu_icon.gif" height="74" width="70" alt="MENU"></p>

        <nav>
          <!--pc表示-->
          <ul id="gnav" class="pc">
            <li><a href="<?php echo site_url('/'); ?>"><img src="<?php echo site_url('/'); ?>common/img/home_off.gif" height="38" width="23" alt="ホーム" onmouseover="this.src='<?php echo site_url('/'); ?>common/img/home_on.gif'" onmouseout="this.src='<?php echo site_url('/'); ?>common/img/home_off.gif'"></a></li>
            <li><a href="<?php echo site_url('/'); ?>hallcake/"><img src="<?php echo site_url('/'); ?>common/img/hcake_off.gif" height="38" width="87" alt="ホールケーキ" onmouseover="this.src='<?php echo site_url('/'); ?>common/img/hcake_on.gif'" onmouseout="this.src='<?php echo site_url('/'); ?>common/img/hcake_off.gif'"></a></li>
            <li><a href="<?php echo site_url('/'); ?>petitgateau/"><img src="<?php echo site_url('/'); ?>common/img/petit_off.gif" height="38" width="83" alt="プチ・ガトー" onmouseover="this.src='<?php echo site_url('/'); ?>common/img/petit_on.gif'" onmouseout="this.src='<?php echo site_url('/'); ?>common/img/petit_off.gif'"></a></li>
            <li><a href="<?php echo site_url('/'); ?>baked/"><img src="<?php echo site_url('/'); ?>common/img/baked_off.gif" height="38" width="59" alt="焼き菓子" onmouseover="this.src='<?php echo site_url('/'); ?>common/img/baked_on.gif'" onmouseout="this.src='<?php echo site_url('/'); ?>common/img/baked_off.gif'"></a></li>
            <li><a href="<?php echo site_url('/'); ?>shop/"><img src="<?php echo site_url('/'); ?>common/img/shop_off.gif" height="38" width="60" alt="店舗案内" onmouseover="this.src='<?php echo site_url('/'); ?>common/img/shop_on.gif'" onmouseout="this.src='<?php echo site_url('/'); ?>common/img/shop_off.gif'"></a></li>
            <li><a href="<?php echo site_url('/'); ?>contact/"><img src="<?php echo site_url('/'); ?>common/img/contact_off.gif" height="38" width="61" alt="お問合せ" onmouseover="this.src='<?php echo site_url('/'); ?>common/img/contact_on.gif'" onmouseout="this.src='<?php echo site_url('/'); ?>common/img/contact_off.gif'"></a></li>
          </ul>

          <!--sp表示-->
          <ul id="menu" class="rsp">
            <li><a href="<?php echo site_url('/'); ?>">ホーム</a></li>
            <li><a href="<?php echo site_url('/'); ?>hallcake/">ホールケーキ</a></li>
            <li><a href="<?php echo site_url('/'); ?>petitgateau/">プチ・ガトー</a></li>
            <li><a href="<?php echo site_url('/'); ?>baked/">焼き菓子</a></li>
            <li><a href="<?php echo site_url('/'); ?>shop/">店舗案内</a></li>
            <li><a href="<?php echo site_url('/'); ?>contact/">お問合せ</a></li>
          </ul>
        </nav>
      </div><!--/.inner-->
    </div><!--/#header-->

<?php if(is_home()): ?>
    <div id="main_img">
      <div class="inner">
        <!--pc表示-->
        <ul class="slider pc">
          <li><img src="<?php echo site_url('/'); ?>common/img/top/main01.jpg" height="484" width="1201" alt="笑顔あふれる幸せをお届けする" class="shadow"></li>
          <li><img src="<?php echo site_url('/'); ?>common/img/top/main02.jpg" height="484" width="1201" alt="笑顔あふれる幸せをお届けする" class="shadow"></li>
          <li><img src="<?php echo site_url('/'); ?>common/img/top/main03.jpg" height="484" width="1201" alt="笑顔あふれる幸せをお届けする" class="shadow"></li>
        </ul>
        <!--sp表示-->
        <p class="rsp"><img src="<?php echo site_url('/'); ?>common/img/sp/main.jpg" height="150" width="375" alt=""></p>
      </div><!--/.inner-->
    </div><!--/#main_img-->

<?php elseif(is_page()): ?>
<?php
  $page = get_post(get_the_ID());
  $slug = $page -> post_name;
  $title = $page -> post_title;
?>
    <div id="btm_main_img">
      <div class="inner">
        <!--spのみ表示-->
        <p class="rsp"><img src="<?php echo site_url('/'); ?>common/img/sp/main.jpg" height="150" width="375" alt="メイン画像"></p>
        <h2><img src="<?php echo site_url('/'); ?>common/img/<?php echo $slug; ?>/page_ttl.png" height="123" width="471" alt="<?php echo $title; ?>"></h2>
      </div><!--/.inner-->
    </div><!--/#btm_main_img-->

<?php elseif(is_post_type_archive(array('hallcake','petitgateau','baked'))): ?>
<?php
  $post_type = get_post_type_object(get_post_type()); //post_typeを取得
  $slug = $post_type->name; //post_type名を取得
  $label = $post_type->label; //ラベル名を取得
?>
    <div id="btm_main_img">
      <div class="inner">
        <!--spのみ表示-->
        <p class="rsp"><img src="<?php echo site_url('/'); ?>common/img/sp/main.jpg" height="150" width="375" alt="メイン画像"></p>
        <h2><img src="<?php echo site_url('/'); ?>common/img/<?php echo $slug; ?>/page_ttl.png" height="123" width="471" alt="<?php echo $label; ?>"></h2>
      </div><!--/.inner-->
    </div><!--/#btm_main_img-->
<?php endif; ?>
  </header>