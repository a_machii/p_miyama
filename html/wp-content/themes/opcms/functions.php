<?php

function call_thissiteurl() {
    return site_url('/');
}
add_shortcode('thissiteurl', 'call_thissiteurl');

function remove_footer_admin () {
  echo 'お問い合わせは<a href="http://www.officepartner.jp/contact/" target="_blank">オフィスパートナー株式会社</a>まで';
}
add_filter('admin_footer_text', 'remove_footer_admin');

if (!current_user_can('administrator')) {
  add_filter('pre_site_transient_update_core', create_function('$a', "return null;"));
}

if (!current_user_can('edit_users')) {
  function remove_menus () {
    global $menu;
    $restricted = array(
      __('リンク'),
      __('ツール'),
      __('コメント'),
      __('プロフィール')
      );
    end ($menu);
    while (prev($menu)){
      $value = explode(' ',$menu[key($menu)][0]);
      if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
        unset($menu[key($menu)]);
      }
    }
  }
  add_action('admin_menu', 'remove_menus');
}

function example_remove_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
  //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');

add_action( 'wp_before_admin_bar_render', 'hide_before_admin_bar_render' );
function hide_before_admin_bar_render() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu( 'wp-logo' );
}

// ウィジェット
//register_sidebar();

// メニュー順番
function custom_menu_order($menu_ord) {
  if (!$menu_ord) return true;
  return array(
    'index.php', // ダッシュボード
    'separator1', // 最初の区切り線
    'edit.php', // 投稿
    'edit.php?post_type=page', // 固定ページ
    'upload.php', // メディア
    'link-manager.php', // リンク
    'edit-comments.php', // コメント
    'separator2', // 二つ目の区切り線
    'themes.php', // 外観
    'plugins.php', // プラグイン
    'users.php', // ユーザー
    'tools.php', // ツール
    'options-general.php', // 設定
    'separator-last', // 最後の区切り線
  );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');

// メニューを非表示にする
function remove_menus02 () {
  if (!current_user_can('level_10')) { //level10以下のユーザーの場合メニューをunsetする
    remove_menu_page('wpcf7'); //Contact Form 7
    global $menu;
    unset($menu[5]); // 投稿
    //unset($menu[20]); // 固定ページ
  }
}
add_action('admin_menu', 'remove_menus02');

//固定ページではビジュアルエディタを利用できないようにする
function disable_visual_editor_in_page(){
  global $typenow;
  if( $typenow == 'page' ){
    add_filter('user_can_richedit', 'disable_visual_editor_filter');
  }
}

function disable_visual_editor_filter(){
  return false;
}
add_action( 'load-post.php', 'disable_visual_editor_in_page' );
add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );

// ログイン画面のロゴ変更
function my_custom_login_logo() {
  echo '<style type="text/css">
  h1 a { background-image:url('.get_bloginfo('template_directory').'/images/logo-login.png) !important; }</style>';
  echo '
  <script type="text/javascript">

  </script>
  ';
}
add_action('login_head', 'my_custom_login_logo');

function lig_wp_category_terms_checklist_no_top( $args, $post_id = null ) {
  $args['checked_ontop'] = false;
  return $args;
}

add_action( 'wp_terms_checklist_args', 'lig_wp_category_terms_checklist_no_top' );

// カスタム投稿の追加
add_action( 'init', 'create_post_type' );
function create_post_type() {
  //ホールケーキ
  register_post_type('hallcake',
    array(
      'label' => 'ホールケーキ',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 05,
      'supports' => array('title'),
      'taxonomies' => array('hallcakecategory'),
      'labels' => array (
        'name' => 'ホールケーキ',
        'all_items' => 'ホールケーキ一覧'
      )
    )
  );

  register_taxonomy(
    'hallcakecategory',
    'hallcake',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'hallcake'),
      'singular_label' => 'ホールケーキカテゴリ'
    )
  );

  //プチ・ガトー
  register_post_type('petitgateau',
    array(
      'label' => 'プチ・ガトー',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 05,
      'supports' => array('title'),
      'taxonomies' => array('petitgateaucategory'),
      'labels' => array (
        'name' => 'プチ・ガトー',
        'all_items' => 'プチ・ガトー一覧'
      )
    )
  );

  register_taxonomy(
    'petitgateaucategory',
    'petitgateau',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'petitgateau'),
      'singular_label' => 'プチ・ガトーカテゴリ'
    )
  );

  //焼き菓子
  register_post_type('baked',
    array(
      'label' => '焼き菓子',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 05,
      'supports' => array('title'),
      'taxonomies' => array('bakedcategory'),
      'labels' => array (
        'name' => '焼き菓子',
        'all_items' => '焼き菓子一覧'
      )
    )
  );

  register_taxonomy(
    'bakedcategory',
    'baked',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'baked'),
      'singular_label' => '焼き菓子カテゴリ'
    )
  );

  //お知らせ
  register_post_type('news',
    array(
      'label' => 'お知らせ',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 05,
      'supports' => array('title'),
      'taxonomies' => array('newscategory'),
      'labels' => array (
        'name' => 'お知らせ',
        'all_items' => 'お知らせ一覧'
      )
    )
  );

  register_taxonomy(
    'newscategory',
    'news',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'news'),
      'singular_label' => 'お知らせカテゴリ'
    )
  );

  //お休み
  register_post_type('holiday',
    array(
      'label' => 'お休み',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 05,
      'supports' => array('title'),
      'taxonomies' => array('holidaycategory'),
      'labels' => array (
        'name' => 'お休み',
        'all_items' => 'お休み一覧'
      )
    )
  );

  register_taxonomy(
    'holidaycategory',
    'holiday',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'holiday'),
      'singular_label' => 'お休みカテゴリ'
    )
  );

  //季節のケーキ 誕生日ケーキ
  register_post_type('cake',
    array(
      'label' => '季節のケーキ・誕生日ケーキ',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 05,
      'supports' => array('title'),
      'taxonomies' => array('cakecategory'),
      'labels' => array (
        'name' => '季節のケーキ・誕生日ケーキ',
        'all_items' => '季節のケーキ・誕生日ケーキ一覧'
      )
    )
  );

  register_taxonomy(
    'cakecategory',
    'cake',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'cake'),
      'singular_label' => '季節のケーキ・誕生日ケーキカテゴリ'
    )
  );

  //おすすめセレクト
  register_post_type('select',
    array(
      'label' => 'おすすめセレクト',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => true,
      'query_var' => false,
      'has_archive' => true,
      'exclude_from_search' => false,
      'menu_position' => 05,
      'supports' => array('title'),
      'taxonomies' => array('selectcategory'),
      'labels' => array (
        'name' => 'おすすめセレクト',
        'all_items' => 'おすすめセレクト一覧'
      )
    )
  );

  register_taxonomy(
    'selectcategory',
    'select',
    array(
      'hierarchical' => true,
      'label' => 'カテゴリ',
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'select'),
      'singular_label' => 'おすすめセレクトカテゴリ'
    )
  );
}

// カスタム分類アーカイブ用のリライトルールを追加する
add_rewrite_rule('hallcake/([^/]+)/page/([0-9]+)/?$', 'index.php?hallcakecategory=$matches[1]&paged=$matches[2]', 'top'); //2ページ目以降用

add_rewrite_rule('petitgateau/([^/]+)/page/([0-9]+)/?$', 'index.php?petitgateaucategory=$matches[1]&paged=$matches[2]', 'top'); //2ページ目以降用

add_rewrite_rule('baked/([^/]+)/page/([0-9]+)/?$', 'index.php?bakedcategory=$matches[1]&paged=$matches[2]', 'top'); //2ページ目以降用

add_rewrite_rule('news/([^/]+)/page/([0-9]+)/?$', 'index.php?newscategory=$matches[1]&paged=$matches[2]', 'top'); //2ページ目以降用

add_rewrite_rule('holiday/([^/]+)/page/([0-9]+)/?$', 'index.php?holidaycategory=$matches[1]&paged=$matches[2]', 'top'); //2ページ目以降用

add_rewrite_rule('cake/([^/]+)/page/([0-9]+)/?$', 'index.php?cakecategory=$matches[1]&paged=$matches[2]', 'top'); //2ページ目以降用

add_rewrite_rule('select/([^/]+)/page/([0-9]+)/?$', 'index.php?selectcategory=$matches[1]&paged=$matches[2]', 'top'); //2ページ目以降用

/* pre get posts 設定
------------------------------------------------------------*/
function customize_main_query($query) {
  if ( is_admin() || ! $query->is_main_query() )
    return;

  if ( $query->is_post_type_archive( 'hallcake') ) {
    $query->set( 'posts_per_page', '-1' );
  }
  if ( $query->is_post_type_archive('petitgateau') ) {
    $query->set( 'posts_per_page', '-1' );
  }
  if ( $query->is_post_type_archive('baked') ) {
    $query->set( 'posts_per_page', '-1' );
  }
}
add_action( 'pre_get_posts', 'customize_main_query' );

/* pagenation
-------------------------------------------------------------*/
function pagination($pages = '', $range = 9){
  $showitems = 1;    //($range * 2)+1;
  global $paged;
  if(empty($paged)) $paged = 1;
  if($pages == ''){
    global $wp_query;
    $pages = $wp_query->max_num_pages;
      if(!$pages){
        $pages = 1;
       }
     }

  if(1 != $pages){
    echo "<div class=\"pager-area\"><ul>";
    //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
    if($paged > 1 && $showitems < $pages) echo "<li class=\"ba\"><a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a></li>";
      for ($i=1; $i <= $pages; $i++){
        if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
          echo ($paged == $i)? "<li class=\"active\"><span class=\"current\">".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a></li>";
        }
      }

    if ($paged < $pages && $showitems < $pages) echo "<li class=\"ba\"><a href=\"".get_pagenum_link($paged + 1)."\">&rsaquo;</a></li>";
    //if ($paged < $pages-1 && $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
    echo "</ul></div>\n";
  }
}

// 画像サイズの変更
add_theme_support('post-thumbnails');
if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'p_thum01', 290, 191, true ); //(cropped)
  add_image_size( 'season_cake', 490, 214, true );
  add_image_size( 'barthday_cake', 224, 205, true );
  add_image_size( 'select', 213, 138, true );
}

// アーカイブ表記に「年」を追加
function my_archives_link($html){
  if(preg_match('/[0-9]+?<\/a>/', $html))
    $html = preg_replace('/([0-9]+?)<\/a>/', '$1年</a>', $html);
  if(preg_match('/title=[\'\"][0-9]+?[\'\"]/', $html))
    $html = preg_replace('/(title=[\'\"][0-9]+?)([\'\"])/', '$1年$2', $html);
  return $html;
}
add_filter('get_archives_link', 'my_archives_link', 10);

// 自動整形を無効にする
add_filter('the_content', 'wpautop_filter', 9);
function wpautop_filter($content) {
  global $post;
  $remove_filter = false;
    $arr_types = array('page'); //自動整形を無効にする投稿タイプを記述
    $post_type = get_post_type( $post->ID );
    if (in_array($post_type, $arr_types)) $remove_filter = true;
    if ( $remove_filter ) {
      remove_filter('the_content', 'wpautop');
      remove_filter('the_excerpt', 'wpautop');
    }
    return $content;
  }

?>