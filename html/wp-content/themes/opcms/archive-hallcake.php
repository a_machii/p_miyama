<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php get_header(); ?>

  <main>
    <div id="contents" class="hpb">
      <div class="inner">
        <ul>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
          <li>
            <p class="photo">
<?php
  $attachment_id = get_field('hc_img');
  $size = "p_thum01"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('hc_img') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
            <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" /></p>
            <p class="name"><?php the_title(); ?></p>
            <?php the_field('hc_price'); ?>
            <p><?php the_field('hc_comment'); ?></p>
<?php $note_id = get_field('hc_note'); ?>
<?php if(!empty($note_id)):?>
            <p class="note"><?php echo $note_id; ?></p>
<?php else: endif;?>
          </li>
<?php endwhile; endif; ?>
        </ul>
      </div><!--/.inner-->
    </div><!--/#contents-->
  </main>

<?php get_footer(); ?>