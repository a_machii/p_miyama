<?php $this_pagetitle = get_post_type_object(get_post_type())->label; ?>
<?php get_header(); ?>

  <main>
    <div id="contents" class="hpb">
      <div class="inner">
        <ul>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
          <li>
            <p class="photo">
<?php
  $attachment_id = get_field('pb_img');
  $size = "p_thum01"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('pb_img') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
            <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" /></p>
            <p class="name"><?php the_title(); ?></p>
            <p class="price"><?php the_field('pb_price'); ?></p>
            <p><?php the_field('pb_comment'); ?></p>
          </li>
<?php endwhile; endif; ?>
        </ul>
      </div><!--/.inner-->
    </div><!--/#contents-->
  </main>

<?php get_footer(); ?>