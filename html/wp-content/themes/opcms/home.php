<?php get_header(); ?>

  <main>
    <section>
      <div id="select">
        <div class="inner">
          <h2 class="pc"><img src="<?php echo site_url('/'); ?>common/img/top/select_ttl.png" height="48" width="1169" alt="おすすめセレクト"></h2>
          <h2 class="rsp"><img src="<?php echo site_url('/'); ?>common/img/sp/select_ttl.png" height="48" width="460" alt=""></h2>

          <ul>
<?php
  $args = array(
    'post_type' => 'select',
    'taxonomy' => 'selectcategory',
    'posts_per_page' => 4
  );
  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ) :
  setup_postdata( $post );
?>
            <li>
              <div class="wrap">
                <p class="pin"><img src="<?php echo site_url('/'); ?>common/img/top/pin.png" height="12" width="12"></p>
<?php
  $attachment_id = get_field('sel_img');
  $size = "select"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('sel_img') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
                <p class="photo"><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php the_title(); ?>" /></p>
              </div><!--/.wrap-->
              <p class="name"><?php the_title(); ?></p>
              <p class="price"><?php the_field('sel_price') ?></p>
            </li>
<?php
  endforeach;
  wp_reset_postdata();
?>
          </ul>
        </div><!--/.inner-->
      </div><!--/#select_section-->
    </section>

<?php
  $args = array(
    'post_type' => 'cake',
    'taxonomy' => 'cakecategory',
  );
  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>

    <div id="cake">
      <div class="inner">
        <section>
          <div class="season">
            <!--pc表示-->
            <h2 class="pc"><img src="<?php echo site_url('/'); ?>common/img/top/season_ttl.png" height="211" width="210" alt="季節のケーキ"></h2>
            <!--sp表示-->
            <h2 class="rsp"><img src="<?php echo site_url('/'); ?>common/img/sp/season_ttl.jpg" height="110" width="236" alt="季節のケーキ"></h2>

<?php
  $attachment_id = get_field('sc_img');
  $size = "season_cake"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
  $attachment = get_post( get_field('sc_img') );
  $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
  $image_title = $attachment->post_title;
?>
            <p class="photo"><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="季節のケーキ" /></p>

            <div class="unit">
              <h3><?php the_field('sc_name'); ?></h3>
              <p class="price"><?php the_field('sc_price'); ?></p>
              <p class="txt"><?php the_field('sc_text'); ?></p>
            </div><!--/.unit-->
          </div><!--/.season-->
        </section>

        <section>
          <div class="birthday">
            <!--pc表示-->
            <h2 class="pc"><img src="<?php echo site_url('/'); ?>common/img/top/birthday_ttl.png" height="213" width="203" alt="誕生日ケーキ"></h2>
            <!--sp表示-->
            <h2 class="rsp"><img src="<?php echo site_url('/'); ?>common/img/sp/birthday_ttl.jpg" height="108" width="236" alt=""></h2>

            <div class="unit">
<?php
  $attachment_id = get_field('bc_img01');
  $size = "barthday_cake"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
?>
              <!--spのみ表示-->
              <p class="photo01 rsp"><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="誕生日ケーキ" /></p>
<?php
  $attachment_id = get_field('bc_img02');
  $size = "barthday_cake"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
?>
              <p class="photo02 rsp"><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="誕生日ケーキ" /></p>

              <p class="price">苺ショート　3,600円＋税</p>
              <p class="price">苺チョコ生ショート　3,900円＋税</p>
              <p class="txt">※お好きなキャラクターをペインティングします<br>
              ※３日前までに要予約<br>
              ※通常のバースデーケーキも承っています。
              </p>
            </div><!--/.unit-->

<?php
  $attachment_id = get_field('bc_img01');
  $size = "barthday_cake"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
?>
            <!--pcのみ表示-->
            <p class="photo01 pc"><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="誕生日ケーキ" /></p>
<?php
  $attachment_id = get_field('bc_img02');
  $size = "barthday_cake"; // (thumbnail, medium, large, full or custom size)
  $image = wp_get_attachment_image_src( $attachment_id, $size );
?>
            <p class="photo02 pc"><img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="誕生日ケーキ" /></p>
<?php
  endforeach;
  wp_reset_postdata();
//cakeのループここまで ?>
          </div><!--/.birthday-->
        </section>
      </div><!--/.inner-->
    </div><!--/#cake_section-->

    <div id="topics">
      <div class="inner">
        <article>
          <div class="news">
            <h2><img src="<?php echo site_url('/'); ?>common/img/top/news_ttl.gif" height="18" width="295" alt="パティスリーミヤマからのお知らせ"></h2>
            <div class="article_area">
<?php
  $args = array(
    'post_type' => 'news',
    'taxonomy' => 'newscategory',
    'posts_per_page' => -1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
              <div class="unit">
                <p class="date"><?php the_time('Y年n月j日') ?></p>
                <p class="txt"><?php the_field('article') ?></p>
              </div><!--/.unit-->

<?php
  endforeach;
  wp_reset_postdata();
?>
            </div><!--/.article_area-->
          </div><!--/.news-->
        </article>

        <article>
          <div class="holiday">
            <h2><img src="<?php echo site_url('/'); ?>common/img/top/holiday_ttl.gif" height="18" width="183" alt="今月のお休みについて"></h2>
            <div class="article_area">
<?php
  $args = array(/* 配列に複数の引数を追加 */
    'post_type' => 'holiday',
    'taxonomy' => 'holidaycategory',
    'posts_per_page' => -1,
  );

  $my_posts = get_posts( $args );
  global $post;
  foreach ( $my_posts as $post ):
  setup_postdata( $post );
?>
              <div class="unit">
                <p class="date"><?php the_time('Y年n月j日') ?></p>
                <p class="txt"><?php the_field('article') ?></p>
              </div><!--/.unit-->

<?php
  endforeach;
  wp_reset_postdata();
?>

            </div><!--/.article_area-->
          </div><!--/.holiday-->
        </article>
      </div><!--/.inner-->
    </div><!--/#topics_section-->
  </main>

<?php get_footer(); ?>